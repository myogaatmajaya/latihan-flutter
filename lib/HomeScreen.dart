import 'package:flutter/material.dart';

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset : false,
      body:SingleChildScrollView (
          padding: const EdgeInsets.symmetric(horizontal: 30.0),
          child:
              Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
            SizedBox(height: 40),
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                IconButton(
                  padding: EdgeInsets.only(right: 180),
                    icon: Icon(Icons.arrow_back),
                    onPressed: () {
                      Navigator.pop(context);
                    }),
                IconButton(icon: Icon(Icons.notifications), onPressed: () {}),
                IconButton(
                    icon: Icon(Icons.add_shopping_cart), onPressed: () {}),
              ],
            ),
            Row(),
            SizedBox(height: 30),
            Text.rich(
              TextSpan(
                  text: 'Welcome,',
                  style: TextStyle(
                      fontWeight: FontWeight.bold, color: Colors.blue.shade400),
                  children: [
                    TextSpan(
                      text: ' Yoga',
                      style: TextStyle(
                          fontWeight: FontWeight.normal,
                          color: Colors.blue.shade900),
                    )
                  ]),
              style: TextStyle(fontSize: 50),
            ),
            SizedBox(height: 30),
            TextField(
                decoration: InputDecoration(
              prefixIcon: Icon(Icons.search, size: 25),
              border:
                  OutlineInputBorder(borderRadius: BorderRadius.circular(20)),
              hintText: 'Search',
            )),
            SizedBox(height: 65),
            Text(
              'Recomended Food',
              style: TextStyle(fontWeight: FontWeight.w600, fontSize: 20),
            ),
            SizedBox(height: 2),
            SizedBox(
              height: 280,
              child: GridView.count(
                crossAxisCount: 2,
                childAspectRatio: 1.480,
                children: [
                  for (var food in foods)
                    Image.asset(
                      'assets/img/$food.jpg',
                    ),
                ],
              ),
            )
          ])),
                 bottomNavigationBar: BottomNavigationBar(
                      items: [
                        BottomNavigationBarItem(
                          icon: Icon(Icons.home),
                          label: 'Home',
                          backgroundColor: Colors.blue,
                        ),
                        BottomNavigationBarItem(
                          icon: Icon(Icons.search),
                          label: 'Search',
                          backgroundColor: Colors.blue,
                        ),
                        BottomNavigationBarItem(
                          icon: Icon(Icons.person),
                          label: 'Account',
                          backgroundColor: Colors.blue,
                        ),
                      ],
                 )
    );
  }
}

final foods = ['sate', 'pizza', 'Fastfood', 'kentang'];
